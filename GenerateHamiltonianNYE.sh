#!/bin/bash
cd ~/processing-3.2.2
path="/home/sejo/Dropbox/Escenaconsejo/Processing/sketchbook/git/HamiltonianCards"
echo "Working in $path..."

for n in {1..17};  
do
	printf -v filename "%02d.png" $n
	printf -v sourcefilename "newyear/%s" $filename
	printf -v outputfilename "results/newyear/%s" $filename
	echo "Processing "$filename $sourcefilename $outputfilename "...";
	./processing-java --sketch=$path --run --save --image $sourcefilename --output $outputfilename --zig-zag --random-grade 10000
	printf -v outputpath "%s/%s" $path $outputfilename
	echo "Cropping " $outputpath "..."
	convert -verbose $outputpath -crop 860x1900+0+0 $outputpath
	echo "Converting to PDF..."
	img2pdf $outputpath --pagesize Letter -o ${outputpath%png}pdf
done

echo "Joining PDFs..."
cd $path"/results/newyear/"
pdftk *.pdf cat output FamilyGame.pdf
cd ~/processing-3.2.2
echo "Done!"
